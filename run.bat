@ECHO off

REM - - - - TIME - - - -
SETLOCAL EnableDelayedExpansion

SET "startTime=%time: =0%"
REM - - - - TIME - - - -

.\__build__\progen.exe --generator sample/Generator/__build__/BasicGenerator.dll --projects sample/ProDesc/Test.prodesc

REM - - - - TIME - - - -
SET "endTime=%time: =0%"

REM Get elapsed time:
SET "end=!endTime:%time:~8,1%=%%100)*100+1!"  &  SET "start=!startTime:%time:~8,1%=%%100)*100+1!"
SET /A "elap=((((10!end:%time:~2,1%=%%100)*60+1!%%100)-((((10!start:%time:~2,1%=%%100)*60+1!%%100)"

REM Convert elapsed time to HH:MM:SS:CC format:
SET /A "cc=elap%%100+100,elap/=100,ss=elap%%60+100,elap/=60,mm=elap%%60+100,hh=elap/60+100"

REM ECHO Start:    %startTime%
REM ECHO End:      %endTime%
ECHO Elapsed:  %hh:~1%%time:~2,1%%mm:~1%%time:~2,1%%ss:~1%%time:~8,1%%cc:~1%
REM - - - - TIME - - - -