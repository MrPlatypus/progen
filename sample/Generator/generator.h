#pragma once

#include "generator.types.h"

#pragma region MACRO - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#ifdef _WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#if defined(__cplusplus)
#define START_SETTER_DECLARATION extern "C" {
#else
#define START_SETTER_DECLARATION
#endif

#define ADD_FILE_SETTER_DECLARATION(SUFFIX) EXPORT enum e_result AddFiles##SUFFIX(void* handle, u32 index, u32 argc, const struct s_directory_view* directoryViews);
#define ADD_SETTER_DECLARATION(NAME) EXPORT enum e_result NAME(void* handle, u32 index, u32 argc, char** argv);

#if defined(__cplusplus)
#define STOP_SETTER_DECLARATION }
#else
#define STOP_SETTER_DECLARATION
#endif

#pragma endregion

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#if defined(__cplusplus)
extern "C"
{
#endif
    
    // Mandatory API
    EXPORT void* CreateProject(const char* proDescWorkingDir, u32 targetCount);
    EXPORT enum e_result Run(void* handle, u32 targetCount);
    EXPORT enum e_result AddDependency(void* handle, u32 targetIndex, const struct s_dependency* dependency);
    EXPORT enum e_result SetterFallback(void* handle, u32 targetIndex, u32 argc, char** argv);
    
#if defined(__cplusplus)
}
#endif

#pragma endregion