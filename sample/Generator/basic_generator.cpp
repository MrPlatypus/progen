#include "basic_generator.h"

#include <fstream>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_compiler_mode
{
	MSVC,
	CLANG
};

struct s_target
{
	// These first 2 are common to all targets
	std::string proDescWorkingDir;
	u32 targetCount;
    
	std::vector<const s_directory_view*> sourceDirectoryViews;
	std::vector<const s_directory_view*> headerDirectoryViews;
	std::vector<const struct s_target*> dependencies;
    
	std::string projectName;
	std::string platformName;
	std::string configurationName;
	e_compiler_mode compilerMode;
	std::vector<std::string> compilerOptions;
	std::vector<std::string> linkerOptions;
};

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void*
CreateProject(const char* proDescWorkingDir, u32 targetCount)
{
	s_target* targets = new s_target[targetCount];
	for (unsigned int i = 0; i < targetCount; ++i)
	{
		targets[i].proDescWorkingDir = proDescWorkingDir;
		targets[i].targetCount = targetCount;
	}
	void* handle = (void*)targets;
	return handle;
}

enum e_result
Run(void* handle, u32 targetCount)
{
	struct s_target* targetArray = (struct s_target*)handle;
	for (u32 i = 0; i < targetCount; ++i)
	{
		struct s_target* const target = targetArray + i;
		//char** sourceFilesArray = targetSourceFilesArrays[i];
        
		std::string filepath = target->proDescWorkingDir + target->projectName + "." + target->platformName + "." + target->configurationName + ".bat";
		std::ofstream myfile(filepath);
		if (myfile.is_open() == false)
			return PG_FAILURE;
        
		{
			myfile << "@ECHO off";
			for (const struct s_target* dependencyTarget : target->dependencies)
			{
				myfile << "\nECHO Depends on: " << dependencyTarget->projectName << " " << dependencyTarget->platformName << " " << dependencyTarget->configurationName;
			}
			myfile << "\npushd ..";
			myfile << "\ncl /nologo /Fo:OUT/ /Fe:OUT/" << target->projectName << ".exe";
			for (const struct s_directory_view* directoryView : target->sourceDirectoryViews)
			{
				for (u32 j = 0; j < directoryView->fileCount; ++j)
				{
					myfile << ' ' << directoryView->path << directoryView->files[j] << std::endl;
				}
			}
			myfile << "\npopd";
			myfile.close();
		}
	}
	return PG_SUCCESS;
}

enum e_result
AddDependency(void* handle, u32 targetIndex, const struct s_dependency* dependency)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	const struct s_target* dependencyTarget = (struct s_target*)(dependency->projectHandle)+dependency->targetIndex;
	target->dependencies.push_back(dependencyTarget);
	return PG_SUCCESS;
}

#pragma region SETTER_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
AddFilesSource(void* handle, u32 targetIndex, u32 argc, const struct s_directory_view* directoryViews)
{
    struct s_target* target = (struct s_target*)(handle)+targetIndex;
	do
	{
		target->sourceDirectoryViews.push_back(directoryViews++);
	} while (--argc > 0);
    return PG_SUCCESS;
}

enum e_result
AddFilesHeader(void* handle, u32 targetIndex, u32 argc, const struct s_directory_view* directoryViews)
{
    struct s_target* target = (struct s_target*)(handle)+targetIndex;
	do
	{
		target->headerDirectoryViews.push_back(directoryViews++);
	} while (--argc > 0);
    return PG_SUCCESS;
}

enum e_result
SetProjectName(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	if (argc != 1)
	{
		return PG_FAILURE;
	}
	target->projectName = *argv;
	return PG_SUCCESS;
}

enum e_result
SetPlatformName(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	if (argc != 1)
	{
		return PG_FAILURE;
	}
	target->platformName = *argv;
	return PG_SUCCESS;
}

enum e_result
SetConfigurationName(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	if (argc != 1)
	{
		return PG_FAILURE;
	}
	target->configurationName = *argv;
	return PG_SUCCESS;
}

enum e_result
SetCompilerMode(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	if (argc != 1)
	{
		return PG_FAILURE;
	}
	if (strcmp("MSVC", *argv) == 0)
	{
		target->compilerMode = e_compiler_mode::MSVC;
	}
	else if (strcmp("CLANG", *argv) == 0)
	{
		target->compilerMode = e_compiler_mode::CLANG;
	}
	else
	{
		return PG_FAILURE;
	}
	return PG_SUCCESS;
}

enum e_result
AddCompilerOptions(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	do
	{
		target->compilerOptions.push_back(*argv);
		++argv;
	} while (--argc > 0);
	return PG_SUCCESS;
}

enum e_result
AddLinkerOptions(void* handle, u32 targetIndex, u32 argc, char** argv)
{
	struct s_target* target = (struct s_target*)(handle)+targetIndex;
	do
	{
		target->linkerOptions.push_back(*argv);
		++argv;
	} while (--argc > 0);
	return PG_SUCCESS;
}

#pragma endregion

#pragma endregion