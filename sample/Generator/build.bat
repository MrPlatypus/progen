@ECHO off

SET PATH_ROOT=%~dp0
SET PATH_BUILD_DIR=%PATH_ROOT%__build__\

SET PATH_SOURCE="basic_generator.cpp"^
                "c_compatibility_test.c"
SET NAME_OUTPUT=BasicGenerator.dll

SET DEFINES=

SET CFLAGS=/nologo /EHsc /Z7 /W4 /wd4100 /Fo:%PATH_BUILD_DIR% %DEFINES%
SET LFLAGS=/nologo /DLL 

IF NOT EXIST %PATH_BUILD_DIR% MKDIR %PATH_BUILD_DIR%

pushd %PATH_ROOT%
cl.exe %CFLAGS% %PATH_SOURCE% /link %LFLAGS% /OUT:%PATH_BUILD_DIR%%NAME_OUTPUT%
popd

REM - - - - DOCUMENTATION - - - -
	REM /nologo Suppresses the display of the copyright banner when the compiler starts up.
	REM /EHsc 	Required for exceptions.
	REM /Z7     generates lighter .pdb files (without this option no .pdb file will be generated)
	REM /W4 	Warning level 4.
	REM /wd4100 Suppresses a warning. Unused variable.
	REM /Fo     Specifies an object (.obj) file name or directory to be used instead of the default.

	REM /link   Arguments specified after this one will be passed to the linker.
	REM /DLL    Produces a .dll.

	REM /OUT    Specifies the output file's name.
REM - - - - DOCUMENTATION - - - -