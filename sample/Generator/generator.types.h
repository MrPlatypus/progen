#pragma once

#include <stdint.h>
#include <stddef.h> // size_t

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef signed char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef float f32;
typedef double f64;

typedef size_t sz;

typedef enum e_result
{
	PG_SUCCESS = 1,
	PG_FAILURE = 0,
} e_result;

typedef struct s_directory_view
{
    const char* path;
    const char** files;
    sz fileCount;
} s_directory_view;

typedef struct s_dependency
{
    void* projectHandle;
    u32 targetIndex;
} s_dependency;

#pragma endregion