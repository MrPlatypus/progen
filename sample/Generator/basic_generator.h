#pragma once

#include "generator.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

START_SETTER_DECLARATION

ADD_FILE_SETTER_DECLARATION(Source)
ADD_FILE_SETTER_DECLARATION(Header)

ADD_SETTER_DECLARATION(SetProjectName)
ADD_SETTER_DECLARATION(SetPlatformName)
ADD_SETTER_DECLARATION(SetConfigurationName)
ADD_SETTER_DECLARATION(SetCompilerMode) // MSVC (.bat), CLANG (.sh)
ADD_SETTER_DECLARATION(AddCompilerOptions)
ADD_SETTER_DECLARATION(AddLinkerOptions)

STOP_SETTER_DECLARATION

#pragma endregion