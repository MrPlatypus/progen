#!/bin/bash 

PATH_ROOT=$(dirname $(readlink -f "$0"))"/"
PATH_BUILD_DIR="$PATH_ROOT""__build__/"

PATH_SOURCE="basic_generator.cpp"
PATH_SOURCE+=" c_compatibility_test.c"
NAME_OUTPUT=BasicGenerator.so

DEFINES=

CFLAGS=-I"$PATH_ROOT" $DEFINES
LFLAGS=-shared

if [ ! -d "$PATH_BUILD_DIR" ]
then
	mkdir "$PATH_BUILD_DIR"
fi

pushd $PATH_ROOT
g++ $CFLAGS $PATH_SOURCE $LFLAGS -o$PATH_BUILD_DIR$NAME_OUTPUT
popd