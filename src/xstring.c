#include "xstring.h"

#include "memory.h"

#include <assert.h> // assert
#include <limits.h> // UINT_MAX
#include <string.h> // strlen
#include <stddef.h> // NULL

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#define INVALID_HASH 0

struct s_xstring_metadata
{
    u32 length;
    u32 hash;
};

#pragma endregion

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static u32
DumbHash(const char* str)
{
    u32 hash = 0;
    while (*str)
    {
        hash = hash + *(str++);
    }
    if (hash == INVALID_HASH) ++hash;
    return hash;
}

static const struct s_xstring_metadata*
XStringMetadata(const xstring xstr)
{
    struct s_xstring_metadata* metadata = (struct s_xstring_metadata*)xstr - 1;
    return metadata;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

xstring
XString(const char str[])
{
    sz strLength = strlen(str);
    assert(strLength <= UINT_MAX);
    
    struct s_xstring_metadata* metadata = Malloc(sizeof(struct s_xstring_metadata) + strLength + 1);
    if (metadata == NULL)
        return NULL;
	
    metadata->length = (u32)strLength;
    metadata->hash = DumbHash(str);
    
    char* xstr = (char*)(metadata + 1);
    memcpy(xstr, str, strLength + 1);
    
    return xstr;
}

void
XStringFree(const xstring xstr)
{
    struct s_xstring_metadata* metadata = (struct s_xstring_metadata*)xstr - 1;
    Free(metadata);
}

xstring
XStringCopy(char mem[], const sz memSize, const char str[])
{
    sz strLength = strlen(str);
    if ((strLength + sizeof(struct s_xstring_metadata) + 1) > memSize)
        return NULL;
    
    struct s_xstring_metadata* metadata = (struct s_xstring_metadata*)mem;
    metadata->length = (u32)strLength;
    metadata->hash = DumbHash(str);
    
    char* xstr = (char*)(metadata + 1);
    memcpy(xstr, str, strLength + 1);
    
    return xstr;
}

xstring
XStringCopyNoHash(char mem[], const sz memSize, const char str[])
{
    sz strLength = strlen(str);
    if ((strLength + sizeof(struct s_xstring_metadata) + 1) > memSize)
        return NULL;
    
    struct s_xstring_metadata* metadata = (struct s_xstring_metadata*)mem;
    metadata->length = (u32)strLength;
    metadata->hash = INVALID_HASH;
    
    char* xstr = (char*)(metadata + 1);
    memcpy(xstr, str, strLength + 1);
    
    return xstr;
}

sz
XStringSize(const xstring xstr)
{
    return XStringMetadata(xstr)->length + sizeof(struct s_xstring_metadata) + 1;
}

u32
XStringLength(const xstring xstr)
{
    return XStringMetadata(xstr)->length;
}

u32
XStringMatch(const xstring xstr1, const xstring xstr2)
{
    const struct s_xstring_metadata* metadata1 = XStringMetadata(xstr1);
    const struct s_xstring_metadata* metadata2 = XStringMetadata(xstr2);
	assert(metadata1->hash != INVALID_HASH);
	assert(metadata2->hash != INVALID_HASH);
	
    if (metadata1->length != metadata2->length ||
        metadata1->hash != metadata2->hash ||
        strcmp(xstr1, xstr2) != 0)
    {
        return RESULT_FALSE;
    }
    return RESULT_TRUE;
}

#pragma endregion