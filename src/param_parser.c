#include "param_parser.h"

#include <stdio.h> // printf
#include <string.h> // strcmp

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

u32
CountParameterValues(char*const* av)
{
	u32 count = 0;
	while (*av != NULL)
	{
		if ((*av)[0] == '-' && (*av)[1] == '-')
			break;
		++count;
		++av;
	}
	return count;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
ParamParse(char*const* av, struct s_params* params)
{
	const char* paramGenerator = "--generator";
	const char* paramProjects = "--projects";
	const char* paramFlags = "--flags";
    const char* paramVerbose = "--verbose";
    
	for (u32 i = 0; av[i] != NULL; ++i)
	{
		if (av[i][0] != '-')
		{
			printf("Unknown parameter: %s\n", av[i]);
			return RESULT_FAILURE;
		}
        
        if (av[i][1] != '-')
        {
            const char* param = av[i] + 1;
            while (*param)
            {
                if (*param == paramVerbose[2])
                {
                    params->verboseEnabled = TRUE;
                }
                else
                {
                    printf("Unknown parameter: '%c' in %s\n", *param, av[i]);
                    return RESULT_FAILURE;
                }
                ++param;
            }
        }
        else
        {
            const char* param = av[i];
            if (strcmp(paramGenerator, param) == 0)
            {
                ++i;
                *(char**)&params->generatorPath = av[i];
                //char** generatorPath = (char**)&params->generatorPath;
                //*generatorPath = av[i];
            }
            else if (strcmp(paramProjects, param) == 0)
            {
                ++i;
                params->projects = av + i;
                params->projectsCount = CountParameterValues(params->projects);
                i += (params->projectsCount - 1);
            }
            else if (strcmp(paramFlags, param) == 0)
            {
                ++i;
                params->flags = av + i;
                params->flagsCount = CountParameterValues(params->flags);
                i += (params->flagsCount - 1);
            }
            else if (strcmp(paramVerbose, param) == 0)
            {
                params->verboseEnabled = TRUE;
            }
            else
            {
                printf("Unknown parameter: %s\n", param);
                return RESULT_FAILURE;
            }
        }
    }
	return RESULT_SUCCESS;
}

#pragma endregion