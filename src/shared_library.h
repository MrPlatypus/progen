#pragma once

#include "shared_library.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result SharedLibraryLoad(const char* libraryPath, s_shared_library* sharedLibrary);
enum e_result SharedLibraryGetSymbol(const s_shared_library* sharedLibrary, const char* symbolName, void** symbol);

#pragma endregion