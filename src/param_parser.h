#pragma once

#include "param_parser.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result ParamParse(char*const* av, struct s_params* params);

#pragma endregion