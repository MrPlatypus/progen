#include "progen.h"

#include "directory_search.h"
#include "file_system.h"
#include "lexer.h"
#include "memory.h"
#include "path.h"
#include "shared_library.h"
#include "xarray.h"
#include "xstring.h"

#include <stdio.h> // printf
#include <stdlib.h> // malloc
#include <string.h> // memset

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct s_directory_view s_directory_view;
typedef struct s_dependency s_dependency;

typedef void* (f_create_project)(const char*, u32);
typedef enum e_result(f_file_setter)(void*, u32, u32, const struct s_directory_view*);
typedef enum e_result(f_setter)(void*, u32, u32, char**);
typedef enum e_result(f_run)(void*, u32);
typedef enum e_result(f_add_dependency)(void*, u32, const struct s_dependency*);

typedef struct s_directory_view_exclusion
{
	xstring frontFilter;
	xstring backFilter;
} s_directory_view_exclusion;

typedef struct s_files_group
{
	xstring group;
	f_file_setter* FileSetter;
	s_directory_view* directoryViews;
	s_directory_view_exclusion* directoryViewExclusions;
} s_files_group;

typedef struct s_project s_project;

typedef struct s_dependency_request
{
	s_project* project;
	xstring platformTag;
	xstring configurationTag;
} s_dependency_request;

typedef struct s_dependency
{
	void* projectHandle;
	u32 targetIndex;
} s_dependency;

typedef struct s_target
{
	u8 isEnabled;
	s_dependency_request* dependencyRequests;
	s_dependency* dependencies;
	s_directory_view* sourceDirectoryViews;
	s_directory_view* headerDirectoryViews;
	s_directory_view_exclusion* directoryViewExclusions;
    s_files_group* filesGroups;
} s_target;

typedef struct s_project
{
	u64 projectHash;
	const char* pathProDescFile;
	const char* pathProDescDirectory;

	u32 platformCount;
	xstring* platformTags;
	u32 configurationCount;
	xstring* configurationTags;
	u32 targetCount;


	void* projectHandle;
	s_directory_search_result* directorySearchResults;
	s_target* targets;

	enum e_lexer_token_type step;
} s_project;

typedef struct s_project_list
{
	s_project* project;
	struct s_project_list* next;
} s_project_list;

typedef struct s_generator
{
	s_shared_library sharedLibrary;
	f_create_project* CreateProject;
	f_run* Run;
	f_add_dependency* AddDependency;
} s_generator;

typedef struct s_progen
{
	s_generator generator;
	s_project_list projectList;

} s_progen;

typedef void (f_load_project)(const char*);
typedef void* (f_get_project)(const char*);

#pragma endregion

s_progen* globalProgen = NULL;

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#pragma region UTILITIES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static u64
Hash(const char* str)
{
	u64 hash = 5381;
	int c;

	while (*str != '\0')
	{
		c = *str;
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
		++str;
	}

	return hash;
}

static void*
MallocZero(sz size)
{
	void* mem = Malloc(size);
	memset(mem, 0, size);
	return mem;
}

static u32
FindProjectTargetIndex(s_project* project, xstring platformTag, xstring configurationTag)
{
	for (u32 targetIndex = 0; targetIndex < project->targetCount; ++targetIndex)
	{
		u32 platformIndex = targetIndex % project->platformCount;
		u32 configurationIndex = targetIndex / project->platformCount;
		if (XStringMatch(platformTag, project->platformTags[platformIndex]) &&
			XStringMatch(configurationTag, project->configurationTags[configurationIndex]))
		{
			return targetIndex;
		}
	}
	printf("/!\\ Matching target [%s, %s] was not found in dependency \"%s\"!\n", platformTag, configurationTag, project->pathProDescFile);
	return 0;
}

#pragma endregion

#pragma region PROGEN_BUILT-IN_FUNCTION - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

static void
AddFilesToDirectoryView(s_files_group* fileGroup, const char* directoryPath, const struct s_directory_search_result* searchResult)
{
#if 1
	s_directory_view* directoryViews = fileGroup->directoryViews;
	{
		u32 directoryViewsCount = XArrayLength(directoryViews);
		for (u32 i = 0; i < directoryViewsCount; ++i)
		{
			if (strcmp(directoryViews[i].path, directoryPath) == 0)
			{
				DirectoryViewPopulate(directoryViews + i, searchResult);
				return;
			}
		}
		directoryViews = XArrayGrow(directoryViews, sizeof(*directoryViews), directoryViewsCount + 1);
		directoryViews[directoryViewsCount] = DirectoryView(directoryPath);
		DirectoryViewPopulate(directoryViews + directoryViewsCount, searchResult);
	}
	fileGroup->directoryViews = directoryViews;

#else
	// Check to see if it is actually any faster
	xstring directoryPathX = XString(directoryPath);
	s_directory_view* directoryViews = fileGroup->directoryViews;
	{
		u32 directoryViewsCount = XArrayLength(directoryViews);
		for (u32 i = 0; i < directoryViewsCount; ++i)
		{
			if (XStringMatch(directoryViews[i].path, directoryPath) == RESULT_TRUE)
			{
				DirectoryViewPopulate(directoryViews + i, searchResult);
				return;
			}
		}
		directoryViews = XArrayGrow(directoryViews, sizeof(*directoryViews), directoryViewsCount + 1);
		directoryViews[directoryViewsCount] = DirectoryView(directoryPath);
		DirectoryViewPopulate(directoryViews + directoryViewsCount, searchResult);
	}
	fileGroup->directoryViews = directoryViews;
#endif
}

static s_files_group*
GetOrCreateFileGroup(s_target* target, const char* group)
{
	s_files_group* filesGroups = target->filesGroups;
	const u32 filesGroupsCount = XArrayLength(filesGroups);
	for (u32 i = 0; i < filesGroupsCount; ++i)
	{
		if (strcmp(group, filesGroups[i].group) == 0)
		{
			return filesGroups + i;
		}
	}
	filesGroups = XArrayGrow(filesGroups, sizeof(*filesGroups), filesGroupsCount + 1);
	filesGroups[filesGroupsCount].group = XString(group);
	filesGroups[filesGroupsCount].FileSetter = NULL;
	filesGroups[filesGroupsCount].directoryViews = NULL;
	filesGroups[filesGroupsCount].directoryViewExclusions = NULL;
	target->filesGroups = filesGroups;
	return filesGroups + filesGroupsCount;
}

static enum e_result
AddFiles(s_project* project, const char* group, u32 argc, char** argv)
{
	u32 directorySearchResultsCount = XArrayLength(project->directorySearchResults);
	project->directorySearchResults = XArrayGrow(project->directorySearchResults, sizeof(*project->directorySearchResults), directorySearchResultsCount + argc);

	u32 index = directorySearchResultsCount;
	while (argc > 0)
	{
		const char* path = PathJoin(project->pathProDescDirectory, *argv);
		const char* directoryPath = PathDirectory(path);
		{
			const char* checkpoint = NULL;
			const char* checkpoint2 = NULL;
			for (sz i = strlen(directoryPath) - 1; path[i]; ++i)
			{
				if (path[i] == '/')
				{
					checkpoint = path + i + 1;
				}
				else if (path[i] == '*')
				{
					*(char*)(path + i) = '\0'; // This is an exception to the const rule !
					checkpoint2 = path + i + 1;
					break;
				}
			}
			const char* frontFilter = (checkpoint == NULL || *checkpoint == '\0') ? NULL : checkpoint;
			const char* backFilter = (checkpoint2 == NULL || *checkpoint2 == '\0') ? NULL : checkpoint2;

			if (DirectoryPathIsValid(directoryPath) == RESULT_TRUE)
			{
				struct s_directory_search_result directorySearchResults = DirectorySearch(directoryPath, frontFilter, backFilter);
				project->directorySearchResults[index++] = directorySearchResults;
				s_target* targets = project->targets;
				const u32 targetsCount = XArrayLength(targets);
				for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
				{
					if (targets[targetIndex].isEnabled == TRUE)
					{
						s_files_group* searchGroup = GetOrCreateFileGroup(targets + targetIndex, group);
						AddFilesToDirectoryView(searchGroup, directoryPath, &directorySearchResults);
					}
				}
			}
			else
			{
				printf("\"%s\" is not a valid directory!\n", directoryPath);
				return RESULT_FAILURE;
			}
		}
		Free(directoryPath);
		Free(path);

		--argc;
		++argv;
	}
	return RESULT_SUCCESS;
}

static void
AddFileExclusions(s_project* project, const char* group, u32 argc, char** argv)
{
	while (argc > 0)
	{
		const char* filter = *argv;
		const char* checkpoint = filter;
		const char* checkpoint2 = NULL;
		for (sz i = 0; filter[i]; ++i)
		{
			if (filter[i] == '/')
			{
				printf("'/' are not currently properly supported in exclusions.\n");
			}
			else if (filter[i] == '*')
			{
				*(char*)(filter + i) = '\0'; // This is an exception to the const rule !
				checkpoint2 = filter + i + 1;
				break;
			}
		}
		xstring frontFilter = (*checkpoint == '\0') ? NULL : XString(checkpoint);
		xstring backFilter = (*checkpoint2 == '\0') ? NULL : XString(checkpoint2);

		s_target* targets = project->targets;
		const u32 targetsCount = XArrayLength(targets);
		for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
		{
			if (targets[targetIndex].isEnabled == TRUE)
			{
				s_files_group* searchGroup = GetOrCreateFileGroup(targets + targetIndex, group);
				s_directory_view_exclusion* directoryViewExclusions = searchGroup->directoryViewExclusions;
				u32 directoryViewExclusionsCount = XArrayLength(directoryViewExclusions);
				directoryViewExclusions = XArrayGrow(directoryViewExclusions, sizeof(*directoryViewExclusions), directoryViewExclusionsCount + 1);
				directoryViewExclusions[directoryViewExclusionsCount].frontFilter = frontFilter;
				directoryViewExclusions[directoryViewExclusionsCount].backFilter = backFilter;
				searchGroup->directoryViewExclusions = directoryViewExclusions;
			}
		}

		--argc;
		++argv;
	}
}

static s_project* LoadProject(const char* pathProDescFile);

static void
AddDependency(s_project* project, u32 argc, char** argv)
{
	if (argc != 3)
	{
		printf("AddDependency: Invalid number of parameters, expected 3.\n");
		return;
	}

	const char* path = PathJoin(project->pathProDescDirectory, argv[0]);
	const char* platform = argv[1];
	const char* configuration = argv[2];

	s_project* dependency = LoadProject(path);

	s_target* targets = project->targets;
	const u32 targetsCount = XArrayLength(targets);
	for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
	{
		if (targets[targetIndex].isEnabled == TRUE)
		{
			s_dependency_request* dependencyRequests = targets[targetIndex].dependencyRequests;
			u32 dependencyRequestsCount = XArrayLength(dependencyRequests);
			dependencyRequests  = XArrayGrow(dependencyRequests, sizeof(*dependencyRequests), dependencyRequestsCount + 1);
			dependencyRequests[dependencyRequestsCount].project = dependency;
			dependencyRequests[dependencyRequestsCount].platformTag = XString(platform);
			dependencyRequests[dependencyRequestsCount].configurationTag = XString(configuration);
			targets[targetIndex].dependencyRequests = dependencyRequests;
		}
	}
}

#pragma endregion

static void
InitializeProjectTargets(s_project* project)
{
	project->targetCount = project->platformCount * project->configurationCount;
	project->targets = XArray(sizeof(*project->targets), project->targetCount);
	memset(project->targets, 0, sizeof(*project->targets) * project->targetCount);
}

static enum e_result
ProcessCategory(const s_lexer_token* lexerToken, s_project* project)
{
	switch (lexerToken->type)
	{
		case LEXER_TOKEN_CATEGORY_HEADER:
		{
			if (project->step != LEXER_TOKEN_NONE)
			{
				printf("HEADER category declared more than once\n");
				return RESULT_FAILURE;
			}
			project->step = LEXER_TOKEN_CATEGORY_HEADER;
		} break;
		case LEXER_TOKEN_CATEGORY_PROPERTIES:
		{
			if (project->step != LEXER_TOKEN_CATEGORY_HEADER)
			{
				printf("Missing mandatory function calls in the HEADER category. Platforms() Configuations()\n");
				return RESULT_FAILURE;
			}
			project->step = LEXER_TOKEN_CATEGORY_PROPERTIES;
		} break;
		default:
		{
		} break;
	}
	return RESULT_SUCCESS;
}

static enum e_result
ProcessFilter(const s_lexer_token* lexerToken, s_project* project)
{
	if (project->step != LEXER_TOKEN_FUNCTION && project->step != LEXER_TOKEN_FILTER && project->step != LEXER_TOKEN_CATEGORY_PROPERTIES)
	{
		printf("Filters can only be declared in the PROPERTIES category\n");
		return RESULT_FAILURE;
	}

	if (lexerToken->paramsCount != 2)
	{
		printf("Invalid number of params passed to filter. Ex: [WIN32,DEBUG]");
		return RESULT_FAILURE;
	}

	xstring platformTag = XString(lexerToken->paramsArray[0]);
	xstring configurationTag = XString(lexerToken->paramsArray[1]);
	{
		s_target* targets = project->targets;
		const u32 targetsCount = XArrayLength(targets);
		for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
		{
			u32 platformIndex = targetIndex % project->platformCount;
			u32 configurationIndex = targetIndex / project->platformCount;

			if ((strcmp("*", platformTag) == 0 || XStringMatch(platformTag, project->platformTags[platformIndex])) &&
				(strcmp("*", configurationTag) == 0 || XStringMatch(configurationTag, project->configurationTags[configurationIndex])))
			{
				targets[targetIndex].isEnabled = TRUE;
			}
			else
			{
				// If the previous step was a fitler then we don't reset the enabled targets.
				if (project->step != LEXER_TOKEN_FILTER)
				{
					targets[targetIndex].isEnabled = FALSE;
				}
			}
		}
	}
	XStringFree(platformTag);
	XStringFree(configurationTag);
	
	project->step = LEXER_TOKEN_FILTER;
	return RESULT_SUCCESS;
}

static enum e_result
ProcessFunction(const s_lexer_token* lexerToken, const s_shared_library* sharedLibrary, s_project* project)
{
	if (project->step == LEXER_TOKEN_NONE)
	{
		printf("Setter outside of a category\n");
	}
	if (project->step == LEXER_TOKEN_CATEGORY_HEADER)
	{
		// TODO: Store these somewhere else so that they aren't recomputed each time
		u64 platformFunctionNameHash = Hash("Platforms");
		u64 configurationFunctionNameHash = Hash("Configurations");
		u64 functionNameHash = Hash(lexerToken->name);
		if (functionNameHash == platformFunctionNameHash)
		{
			if (lexerToken->paramsCount == 0)
			{
				printf("Invalid number of params passed to Platform(...)\n");
				return RESULT_FAILURE;
			}
			if (project->platformCount != 0)
			{
				printf("Platform has already been set");
				return RESULT_FAILURE;
			}
			project->platformCount = lexerToken->paramsCount;

			project->platformTags = XArray(sizeof(xstring), lexerToken->paramsCount);
			for (u32 i = 0; i < lexerToken->paramsCount; ++i)
			{
				project->platformTags[i] = XString(lexerToken->paramsArray[i]);
			}
		}
		else if (functionNameHash == configurationFunctionNameHash)
		{
			if (lexerToken->paramsCount == 0)
			{
				printf("Invalid number of params passed to Platform(...)\n");
				return RESULT_FAILURE;
			}
			if (project->configurationCount != 0)
			{
				printf("Platform has already been set");
				return RESULT_FAILURE;
			}
			project->configurationCount = lexerToken->paramsCount;

			project->configurationTags = XArray(sizeof(xstring), lexerToken->paramsCount);
			for (u32 i = 0; i < lexerToken->paramsCount; ++i)
			{
				project->configurationTags[i] = XString(lexerToken->paramsArray[i]);
			}
		}
		else
		{
			printf("Function name matched none of the authorised built-in function of the Header category.");
			return RESULT_FAILURE;
		}

		if (project->platformCount != 0 && project->configurationCount != 0)
		{
			InitializeProjectTargets(project);
		}
		// We don't set the step to function when processing the mandatory function to not introduce a possible failpoint if we have a filter after the first mandatory function
		//project->step = LEXER_TOKEN_FUNCTION;
	}
	else
	{
		if (strncmp(lexerToken->name, "AddFiles", 8) == 0)
		{
			const char* group = lexerToken->name + 8;
			f_file_setter* FileSetter = NULL;
			if (SharedLibraryGetSymbol(sharedLibrary, lexerToken->name, (void**)&FileSetter) == RESULT_FAILURE)
			{
				printf("%s not implemented by generator\n", lexerToken->name);
				return RESULT_SUCCESS;
			}

			s_target* targets = project->targets;
			const u32 targetsCount = XArrayLength(targets);
			for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
			{
				if (targets[targetIndex].isEnabled == TRUE)
				{
					s_files_group* searchGroup = GetOrCreateFileGroup(targets + targetIndex, group);
					searchGroup->FileSetter = FileSetter;
				}
			}

			AddFiles(project, group, lexerToken->paramsCount, lexerToken->paramsArray);
		}
		else if (strncmp(lexerToken->name, "AddFileExclusions", 17) == 0)
		{
			const char* group = lexerToken->name + 17;
			AddFileExclusions(project, group, lexerToken->paramsCount, lexerToken->paramsArray);
		}
		else if (strcmp(lexerToken->name, "AddDependency") == 0)
		{
			AddDependency(project, lexerToken->paramsCount, lexerToken->paramsArray);
		}
		else
		{
			f_setter* Setter = NULL;
			if (SharedLibraryGetSymbol(sharedLibrary, lexerToken->name, (void**)&Setter) == RESULT_FAILURE)
			{
				printf("%s not implemented by generator\n", lexerToken->name);
				return RESULT_SUCCESS;
			}
			s_target* targets = project->targets;
			const u32 targetsCount = XArrayLength(targets);
			for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
			{
				if (targets[targetIndex].isEnabled == TRUE)
				{
					if (Setter(project->projectHandle, targetIndex, lexerToken->paramsCount, lexerToken->paramsArray) == RESULT_FAILURE)
						return RESULT_FAILURE;
				}
			}
		}
		project->step = LEXER_TOKEN_FUNCTION;
	}
	return RESULT_SUCCESS;
}

static enum e_result
ProcessProject(s_generator* generator, s_project* project)
{
	s_file file; memset(&file, 0, sizeof(file));
	if (FileOpen(project->pathProDescFile, &file) == RESULT_FAILURE)
		return RESULT_FAILURE;
	s_file_buffer fileBuffer; memset(&fileBuffer, 0, sizeof(fileBuffer));
	if (FileRead(&file, &fileBuffer) == RESULT_FAILURE)
		return RESULT_FAILURE;

	s_lexer lexer; memset(&lexer, 0, sizeof(lexer));
	if (LexerInit(&fileBuffer, &lexer) == RESULT_FAILURE)
		return RESULT_FAILURE;

	s_lexer_token lexerToken; memset(&lexerToken, 0, sizeof(lexerToken));
	while (lexerToken.type != LEXER_TOKEN_EOF)
	{
		if (LexerGetNextToken(&lexer, &lexerToken) == RESULT_FAILURE)
			return RESULT_FAILURE;
		switch (lexerToken.type)
		{
			case LEXER_TOKEN_CATEGORY_HEADER:
			case LEXER_TOKEN_CATEGORY_PROPERTIES:
			{
				if (ProcessCategory(&lexerToken, project) == RESULT_FAILURE)
					return RESULT_FAILURE;
				if (project->step == LEXER_TOKEN_CATEGORY_PROPERTIES)
				{
					project->projectHandle = generator->CreateProject(project->pathProDescDirectory, project->targetCount);
				}
			} break;
			case LEXER_TOKEN_FILTER:
			{
				if (ProcessFilter(&lexerToken, project) == RESULT_FAILURE)
					return RESULT_FAILURE;
			} break;
			case LEXER_TOKEN_FUNCTION:
			{
				if (ProcessFunction(&lexerToken, &generator->sharedLibrary, project) == RESULT_FAILURE)
					return RESULT_FAILURE;
			} break;
			case LEXER_TOKEN_EOF:
			{
			} break;
			default:
			{
				printf("%s\n", lexerToken.name);
			} break;
		}
	}

	// Once everything is done then we can send the directory views (aka source files) to the generator
	if (XArrayLength(project->directorySearchResults) > 0)
	{
		s_target* targets = project->targets;
		const u32 targetsCount = XArrayLength(targets);
		for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
		{
            s_files_group* filesGroups = targets[targetIndex].filesGroups;
            const u32 filesGroupsCount = XArrayLength(filesGroups);
            
            for (u32 i = 0; i < filesGroupsCount; ++i)
            {
                s_directory_view* directoryViews = filesGroups[i].directoryViews;
				if (directoryViews != NULL)
				{
	                const u32 directoryViewsCount = XArrayLength(directoryViews);
					for(u32 j = 0; j < directoryViewsCount; ++j)
					{
						s_directory_view_exclusion* directoryViewExclusions = filesGroups[i].directoryViewExclusions;
						const u32 directoryViewExclusionsCount = XArrayLength(directoryViewExclusions);
						for (u32 k = 0; k < directoryViewExclusionsCount; ++k)
						{
							DirectoryViewExclude(directoryViews + j, directoryViewExclusions[k].frontFilter, directoryViewExclusions[k].backFilter);
						}
					}
					
					if (filesGroups[i].FileSetter(project->projectHandle, targetIndex, directoryViewsCount, directoryViews) == RESULT_FAILURE)
						return RESULT_FAILURE;
				}
            }
        }
    }
    return RESULT_SUCCESS;
}

static void
ProcessDependencies(s_generator* generator, s_project* project)
{
	s_target* targets = project->targets;
	const u32 targetsCount = XArrayLength(targets);
	for (u32 targetIndex = 0; targetIndex < targetsCount; ++targetIndex)
	{
		s_target* target = project->targets + targetIndex;
		s_dependency_request* dependencyRequests = target->dependencyRequests;
		const u32 dependencyRequestsCount = XArrayLength(dependencyRequests);
		
		if (dependencyRequestsCount == 0)
			continue;

		s_dependency* dependencies = target->dependencies;
		dependencies = XArrayGrow(dependencies, sizeof(*dependencies), dependencyRequestsCount);
		for (u32 i = 0; i < dependencyRequestsCount; ++i)
		{
			s_dependency_request* dependencyRequest = dependencyRequests + i;
			s_dependency* dependency = dependencies + i;
			dependency->projectHandle = dependencyRequest->project->projectHandle;

			u32 platformIndex = targetIndex % project->platformCount;
			u32 configurationIndex = targetIndex / project->platformCount;

			xstring platformTag = strcmp("*", dependencyRequest->platformTag) == 0 ? project->platformTags[platformIndex] : dependencyRequest->platformTag;
			xstring configurationTag = strcmp("*", dependencyRequest->configurationTag) == 0 ? project->configurationTags[configurationIndex] : dependencyRequest->configurationTag;
			
			dependency->targetIndex = FindProjectTargetIndex(dependencyRequest->project, platformTag, configurationTag);

			generator->AddDependency(project->projectHandle, targetIndex, dependencies + i);
		}
		target->dependencies = dependencies;
	}
}

static enum e_result
LoadAPI(s_progen* progen, const char* generatorPath)
{
	s_generator* generator = &progen->generator;
	if (SharedLibraryLoad(generatorPath, &generator->sharedLibrary) == RESULT_FAILURE)
	{
		printf("[API] Failed to load generator as a shared library\n");
		return RESULT_FAILURE;
	}

	if (SharedLibraryGetSymbol(&generator->sharedLibrary, "CreateProject", (void**)&generator->CreateProject) == RESULT_FAILURE)
	{
		printf("[API] Failed to load \"CreateProject\"\n");
		return RESULT_FAILURE;
	}
	if (SharedLibraryGetSymbol(&generator->sharedLibrary, "Run", (void**)&generator->Run) == RESULT_FAILURE)
	{
		printf("[API] Failed to load \"Run\"\n");
		return RESULT_FAILURE;
	}
	if (SharedLibraryGetSymbol(&generator->sharedLibrary, "AddDependency", (void**)&generator->AddDependency) == RESULT_FAILURE)
	{
		printf("[API] Failed to load \"AddDependencies\"\n");
		return RESULT_FAILURE;
	}

	return RESULT_SUCCESS;
}

static s_project*
LoadProject(const char* pathProDescFile)
{
	// Add a project to be loaded, if it isn't already in the list
	u64 hash = Hash(pathProDescFile);
	s_project_list* list = &globalProgen->projectList;
	while (list->project != NULL)
	{
		s_project* project = globalProgen->projectList.project;
		if (project->projectHash == hash)
		{
			return project;
		}
		list = list->next;
	}
	s_project* project = MallocZero(sizeof(s_project));
	{
		project->projectHash = hash;
		project->pathProDescFile = PathAbsolute(pathProDescFile);
		project->pathProDescDirectory = PathDirectory(pathProDescFile);
	}
	list->project = project;
	list->next = MallocZero(sizeof(s_project_list));
	return project;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
ProGenRun(struct s_params* params)
{
	s_progen progen; memset(&progen, 0, sizeof(progen));
	globalProgen = &progen;

	if (LoadAPI(&progen, params->generatorPath) == RESULT_FAILURE)
		return RESULT_FAILURE;

	if (params->verboseEnabled)
	{
		printf("Generator [%s] loaded successfuly\n", params->generatorPath);
	}

	// Starting actual program
	progen.projectList.project = NULL;

	for (u32 i = 0; i < params->projectsCount; ++i)
	{
		const char* pathAbsolute = PathAbsolute(params->projects[i]);
		if (params->verboseEnabled)
			printf("Loading [%s]\n", pathAbsolute);
		LoadProject(pathAbsolute);
		if (params->verboseEnabled)
			printf("  DONE\n");
		Free(pathAbsolute);
	}

	// Process ProDesc
	for (s_project_list* list = &progen.projectList; list->project != NULL; list = list->next)
	{
		s_project* project = list->project;
		if (params->verboseEnabled)
			printf("Processing [%s]\n", project->pathProDescFile);
		if (ProcessProject(&progen.generator, project) != RESULT_SUCCESS)
			return RESULT_FAILURE;
		if (params->verboseEnabled)
			printf("  DONE\n");
	}
	
	// Resolve dependencies
	for (s_project_list* list = &progen.projectList; list->project != NULL; list = list->next)
	{
		s_project* project = list->project;
		if (params->verboseEnabled)
			printf("Resolving dependencies [%s]\n", project->pathProDescFile);
		ProcessDependencies(&progen.generator, project);
		if (params->verboseEnabled)
			printf("  DONE\n");
	}
	
	// Run generator for each ProDesc
	for (s_project_list* list = &progen.projectList; list->project != NULL; list = list->next)
	{
		s_project* project = list->project;
		if (params->verboseEnabled)
			printf("Running Generator [%s]\n", project->pathProDescFile);
		if (progen.generator.Run(project->projectHandle, project->targetCount) == RESULT_FAILURE)
			return RESULT_FAILURE;
		if (params->verboseEnabled)
			printf("  DONE\n");
	}
	
	return RESULT_SUCCESS;
}

#pragma endregion