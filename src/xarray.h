#pragma once

#include "common.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void* XArray(u32 elementSize, const u32 length);
void* XArrayGrow(void* array, u32 elementSize, const u32 length); // You can pass NULL and XArray will be called instead
u32 XArrayLength(const void* array);
void XArrayFree(const void* array);

#pragma endregion