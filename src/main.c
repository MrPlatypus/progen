#include "param_parser.h"

#include "progen.h"

#include <stdio.h> // printf

int
main(int ac, char** av)
{
	(void)ac;
	static struct s_params params; // Static so that the memory is set to 0
	if (ParamParse(av + 1, &params) == RESULT_FAILURE)
		return RESULT_EXIT_FAILURE;

	if (params.generatorPath == NULL)
	{
		printf("Missing required param \"--generator\"\n");
		return RESULT_EXIT_FAILURE;
	}

	printf("Generator path: %s\n", params.generatorPath);
	if (params.projects == NULL || params.projectsCount == 0)
	{
		printf("Missing required param \"--projects\"\n");
		return RESULT_EXIT_FAILURE;
	}

	printf("Projects:\n");
	for (u32 i = 0; i < params.projectsCount; ++i)
	{
		printf("  %s\n", params.projects[i]);
	}

	if (params.flags != NULL)
	{
		printf("Flags:\n");
		for (u32 i = 0; i < params.flagsCount; ++i)
		{
			printf("  %s\n", params.flags[i]);
		}
	}

	if (ProGenRun(&params) == RESULT_FAILURE)
		return RESULT_EXIT_FAILURE;
	return RESULT_EXIT_SUCCESS;
}