#pragma once

#include "file_system.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_file_system_result FileOpen(const char* path, s_file* file);
enum e_file_system_result FileRead(const s_file* file, s_file_buffer* fileBuffer);

#pragma endregion