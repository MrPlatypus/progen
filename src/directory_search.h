#pragma once

#include "directory_search.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result DirectoryPathIsValid(const char directoryPath[]);

struct s_directory_search_result DirectorySearch(const char directoryPath[], const char* frontFilter, const char* backFilter);
void DirectorySearchFree(struct s_directory_search_result* result);

struct s_directory_view DirectoryView(const char* path);
void DirectoryViewPopulate(struct s_directory_view* view, const struct s_directory_search_result* result);
void DirectoryViewExclude(struct s_directory_view* view, xstring frontFilter, xstring backFilter);
void DirectoryViewFree(struct s_directory_view* view);

#pragma endregion