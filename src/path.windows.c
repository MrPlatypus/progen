#include "path.h"

#include "memory.h"

#include <assert.h> // assert
#define _AMD64_ // required by Windows
#define WIN32_LEAN_AND_MEAN // Helps to lighten some of Windows's headers
#include "processenv.h"

#pragma region PRIVATE_FUNCTIONS

static const char*
PathNormalize(const char* path)
{
	(void)path;
	return NULL;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

const char*
PathAbsolute(const char* pathRelative)
{
	static char* pathWorkingDirectory;
	static DWORD pathWorkingDirectoryLength;
	if (pathWorkingDirectory == NULL)
	{
		pathWorkingDirectoryLength = GetCurrentDirectoryA(0, NULL);
		pathWorkingDirectory = Malloc(pathWorkingDirectoryLength + 1);
		GetCurrentDirectoryA(pathWorkingDirectoryLength, pathWorkingDirectory);
		pathWorkingDirectory[pathWorkingDirectoryLength - 1] = '/';
		pathWorkingDirectory[pathWorkingDirectoryLength] = '\0';
		for (sz i = 0; i < pathWorkingDirectoryLength; ++i)
		{
			if (pathWorkingDirectory[i] == '\\')
				pathWorkingDirectory[i] = '/';
		}
	}
	sz pathRelativeLength = strlen(pathRelative);
	char* pathFull = NULL;
	if ((pathRelative[0] >= 'A' && pathRelative[0] <= 'Z') && pathRelative[1] == ':')
	{
		pathFull = Malloc(pathRelativeLength + 1);
		memcpy(pathFull, pathRelative, pathRelativeLength);
		pathFull[pathRelativeLength] = '\0';
		for (sz i = 0; i < pathWorkingDirectoryLength; ++i)
		{
			if (pathWorkingDirectory[i] == '\\')
				pathWorkingDirectory[i] = '/';
		}
	}
	else
	{
		pathFull = Malloc(pathWorkingDirectoryLength + pathRelativeLength + 1);
		memcpy(pathFull, pathWorkingDirectory, pathWorkingDirectoryLength);
		memcpy(pathFull + pathWorkingDirectoryLength, pathRelative, pathRelativeLength);
		pathFull[pathWorkingDirectoryLength + pathRelativeLength] = '\0';
		for (sz i = 0; i < pathWorkingDirectoryLength; ++i)
		{
			if (pathWorkingDirectory[i] == '\\')
				pathWorkingDirectory[i] = '/';
		}
	}
	return pathFull;
}

const char*
PathDirectory(const char* pathFile)
{
	sz pathLength = strlen(pathFile);
	sz checkpoint = 0;
	for (sz i = 0; i < pathLength; ++i)
	{
		if (pathFile[i] == '/')
			checkpoint = i + 1;
	}
	char* pathDirectory = NULL;
	if (checkpoint == 0)
	{
		assert(0 && "Path should be absolute and therefor should contain at least one seperator !");
	}
	else
	{
		pathDirectory = Malloc(checkpoint + 1);
		memcpy(pathDirectory, pathFile, checkpoint);
		pathDirectory[checkpoint] = '\0';
	}
	return pathDirectory;
}

const char*
PathJoin(const char* pathLeft, const char* pathRight)
{
	sz pathLeftLength = strlen(pathLeft);
	sz pathRightLength = strlen(pathRight);
	char* pathFusion = Malloc(pathLeftLength + pathRightLength + 1);
	memcpy(pathFusion, pathLeft, pathLeftLength);
	memcpy(pathFusion + pathLeftLength, pathRight, pathRightLength);
	pathFusion[pathLeftLength + pathRightLength] = '\0';
	for (sz i = pathLeftLength; i < pathLeftLength + pathRightLength; ++i)
	{
		if (pathFusion[i] == '\\')
			pathFusion[i] = '/';
	}
	return pathFusion;
}

#pragma endregion