#pragma once

#include "common.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_file_system_result
{
	FS_SUCCESS = RESULT_SUCCESS,
	FS_FAILURE = RESULT_FAILURE,
	FS_FILE_NOT_FOUND = 2
};

typedef struct s_file
{
	void* handle;
} s_file;

typedef struct s_file_buffer
{
	char* data;
	sz size;
} s_file_buffer;

#pragma endregion