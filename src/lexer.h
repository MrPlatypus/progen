#pragma once

#include "lexer.types.h"

#include "file_system.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result LexerInit(const s_file_buffer* fileBuffer, s_lexer* lexer);
enum e_result LexerGetNextToken(s_lexer* lexer, s_lexer_token* lexerToken);

#pragma endregion