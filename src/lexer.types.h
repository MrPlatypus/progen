#pragma once

#include "common.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct s_lexer
{
	char* start;
	char* cursor; // The current read cursor
} s_lexer;

typedef enum e_lexer_token_type
{
	LEXER_TOKEN_NONE = 0,
	LEXER_TOKEN_CATEGORY_HEADER,
	LEXER_TOKEN_CATEGORY_PROPERTIES,
	LEXER_TOKEN_FILTER,
	LEXER_TOKEN_FUNCTION,
	LEXER_TOKEN_EOF
} e_lexer_token_type;

typedef struct s_lexer_token
{
	e_lexer_token_type type;
	void* memory;
	char* name;
	char** paramsArray;
	u32 paramsCount;
} s_lexer_token;

#pragma endregion