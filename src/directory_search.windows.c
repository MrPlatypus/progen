#include "directory_search.h"

#include "memory.h"
#include "xstring.h"

#include <stdio.h> //printf
#include <string.h> // memcpy

#define _AMD64_ // required by Windows
#define WIN32_LEAN_AND_MEAN // Helps to lighten some of Windows's headers
#include <fileapi.h> // FindFirstFile, FindNextFile
#include <handleapi.h> // INVALID_HANDLE_VALUE
#include <stringapiset.h> // MultiByteToWideChar, WideCharToMultiByte

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static enum e_result
StringMatch(const char* str1, const char* str2, i32 len)
{
    while (*(str1++) == *(str2++) && len--);
    if (len == 0)
    {
        return RESULT_SUCCESS;
    }
    return RESULT_FAILURE;
}

static WCHAR*
utf8ToUtf16(const char* utf8)
{
    i32 utf16Len = MultiByteToWideChar(CP_ACP, 0, utf8, -1, NULL, 0);
    WCHAR* utf16 = Malloc(sizeof(WCHAR) * utf16Len);
    MultiByteToWideChar(CP_ACP, 0, utf8, -1, utf16, utf16Len);
    return utf16;
}

static char*
utf16ToUtf8(const WCHAR* utf16)
{
    i32 utf8Len = WideCharToMultiByte(CP_ACP, 0, utf16, -1, NULL, 0, 0, 0);
    char* utf8 = Malloc(utf8Len);
    WideCharToMultiByte(CP_ACP, 0, utf16, -1, utf8, utf8Len, 0, 0);
    return utf8;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
DirectoryPathIsValid(const char directoryPath[])
{
    enum e_result result = RESULT_TRUE;

    WCHAR* utf16 = utf8ToUtf16(directoryPath);
    {
        DWORD dwFileAttributes = GetFileAttributesW(utf16);
        if (dwFileAttributes == INVALID_FILE_ATTRIBUTES || !(dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
            result = RESULT_FALSE;
        }
    }
    Free(utf16);
    return RESULT_TRUE;
}

struct s_directory_search_result
DirectorySearch(const char directoryPath[], const char* frontFilter, const char* backFilter)
{
    static const sz arenaChunkSize = KILOBYTES(1);
    
	// Size == bytes
    
	sz arrayIndex = 0;
	sz arrayCurrentSize = 0;
	sz arrayMaxSize = arenaChunkSize;
	sz* stringsOffset = Malloc(arrayMaxSize);
    
	sz bufferCurrentSize = 0;
	sz bufferMaxSize = arenaChunkSize;
	char* buffer = Malloc(bufferMaxSize);

    char* pathFull = NULL;
    if (frontFilter == NULL && backFilter == NULL)
    {
        pathFull = (char*)directoryPath;
    }
    else if (frontFilter == NULL && backFilter != NULL)
    {
        sz directoryPathLength = strlen(directoryPath);
        sz backFilterLength = strlen(backFilter);
        pathFull = Malloc(directoryPathLength + backFilterLength + 2); // 1 for \0 and 1 for *
        memcpy(pathFull, directoryPath, directoryPathLength);
        pathFull[directoryPathLength] = '*';
        memcpy(pathFull + directoryPathLength + 1, backFilter, backFilterLength + 1);
    }
    else if (frontFilter != NULL && backFilter == NULL)
    {
        sz directoryPathLength = strlen(directoryPath);
        sz frontFilterLength = strlen(frontFilter);
        pathFull = Malloc(directoryPathLength + frontFilterLength + 2); // 1 for \0 and 1 for *
        memcpy(pathFull, directoryPath, directoryPathLength);
        memcpy(pathFull + directoryPathLength, frontFilter, frontFilterLength);
        pathFull[directoryPathLength + frontFilterLength] = '*';
        pathFull[directoryPathLength + frontFilterLength + 1] = '\0';
    }
    else
    {
        sz directoryPathLength = strlen(directoryPath);
        sz frontFilterLength = strlen(frontFilter);
        sz backFilterLength = strlen(backFilter);
        pathFull = Malloc(directoryPathLength + frontFilterLength + backFilterLength + 2); // 1 for \0 and 1 for *
        memcpy(pathFull, directoryPath, directoryPathLength);
        memcpy(pathFull + directoryPathLength, frontFilter, frontFilterLength);
        pathFull[directoryPathLength + frontFilterLength] = '*';
        memcpy(pathFull + directoryPathLength + frontFilterLength + 1, backFilter, backFilterLength + 1);
    }

    WCHAR* utf16 = utf8ToUtf16(pathFull);
    {
        WIN32_FIND_DATAW findFileData;
        HANDLE handle = FindFirstFileW(utf16, &findFileData);
        do
        {
            if (arrayCurrentSize >= arrayMaxSize)
            {
                arrayMaxSize += arenaChunkSize;
                stringsOffset = Realloc(stringsOffset, arrayMaxSize);
            }
            {
                char* utf8 = utf16ToUtf8(findFileData.cFileName);
                {
                    xstring xstr;
                    while ((xstr = XStringCopyNoHash(buffer + bufferCurrentSize, bufferMaxSize - bufferCurrentSize, utf8)) == NULL)
                    {
                        bufferMaxSize += arenaChunkSize;
                        buffer = Realloc(buffer, bufferMaxSize);
                    }
                    bufferCurrentSize += XStringSize(xstr); 
                    
                    ptrdiff_t offset = xstr - buffer;
                    stringsOffset[arrayIndex++] = offset;
                    arrayCurrentSize += sizeof(char*);
                }
                Free(utf8);
            }
        }
        while (FindNextFileW(handle, &findFileData) != 0);
        FindClose(handle);
    }
    Free(utf16);
    
    struct s_directory_search_result result;
    result.mem = NULL;
    result.files = NULL;
    result.fileCount = 0;
    
    // "Shrinking"
    if (arrayIndex > 0)
    {
        result.mem = Malloc(bufferCurrentSize);
        memcpy(result.mem, buffer, bufferCurrentSize);
        Free(buffer);
        
        result.files = Malloc(arrayIndex * sizeof(char*));
        for (sz i = 0; i < arrayIndex; ++i)
        {
            result.files[i] = (xstring)((u8*)result.mem + stringsOffset[i]);
        }
        Free(stringsOffset);
        
        result.fileCount = arrayIndex;
    }
    else
    {
        Free(buffer);
        Free(stringsOffset);
    }

    if (pathFull != directoryPath)
    {
        Free(pathFull);
    }
    
    return result;
}

void
DirectorySearchFree(struct s_directory_search_result* result)
{
    if (result->fileCount > 0)
    {
        Free(result->mem);
        result->mem = NULL;
        Free(result->files); // xstring are actually const char*, and they can't be implicitly converted to void*
        result->files = NULL;
    }
}

struct s_directory_view
DirectoryView(const char* path)
{
    struct s_directory_view view;
    view.files = NULL;
    view.fileCount = 0;
    view.path = XString(path);
    return view;
}

void
DirectoryViewPopulate(struct s_directory_view* view, const struct s_directory_search_result* result)
{   
    view->files = Realloc(view->files, sizeof(char*) * (view->fileCount + result->fileCount));
    memcpy((char**)view->files + view->fileCount, result->files, sizeof(char*) * result->fileCount);
    view->fileCount += result->fileCount;
}

void
DirectoryViewExclude(struct s_directory_view* view, xstring frontFilter, xstring backFilter)
{
    if (frontFilter == NULL && backFilter == NULL)
    {
        return;
    }
    else if (frontFilter == NULL && backFilter != NULL)
    {
        u32 backFilterLength = XStringLength(backFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < backFilterLength)
            {
                ++i;
                continue;
            }
            
            if (strcmp(file + (stringLength - backFilterLength), backFilter) != 0)
            {
                ++i;
                continue;
            }
            
            --view->fileCount;
            view->files[i] = view->files[view->fileCount];
        }
    }
    else if (frontFilter != NULL && backFilter == NULL)
    {
        u32 frontFilterLength = XStringLength(frontFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < frontFilterLength)
            {
                ++i;
                continue;
            }
            
            if (StringMatch(file, frontFilter, frontFilterLength) == RESULT_FAILURE)
            {
                ++i;
                continue;
            }
            
            --view->fileCount;
            view->files[i] = view->files[view->fileCount];
        }
    }
    else
    {
        u32 frontFilterLength = XStringLength(frontFilter);
        u32 backFilterLength = XStringLength(backFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < (frontFilterLength + backFilterLength))
            {
                ++i;
                continue;
            }
            
            if (strcmp(file + (stringLength - backFilterLength), backFilter) != 0)
            {
                ++i;
                continue;
            }
            
            if (StringMatch(file, frontFilter, frontFilterLength) == RESULT_FAILURE)
            {
                ++i;
                continue;
            }
            
            view->files[i] = view->files[view->fileCount - 1];
            view->fileCount -= 1;
        }
    }
}

void
DirectoryViewFree(struct s_directory_view* view)
{
    XStringFree(view->path);
    view->path = NULL;
    if (view->fileCount > 0)
    {
        Free(view->files);
        view->files = NULL;
        view->fileCount = 0;
    }
};

#pragma endregion