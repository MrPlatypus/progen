#pragma once

#include "common.h"

//#define DEBUG

#define Malloc(size) _Malloc(__FILE__, __FUNCTION__, __LINE__, size)
#define Realloc(mem, size) _Realloc(__FILE__, __FUNCTION__, __LINE__, mem, size)
#define Free(mem) _Free(__FILE__, __FUNCTION__, __LINE__, mem)

void* _Malloc(char*const file, const char* function, u32 line, sz size);
void* _Realloc(char*const file, const char* function, u32 line, const void* mem, sz size);
void _Free(char*const file, const char* function, u32 line, const void* mem);