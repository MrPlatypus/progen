#include "shared_library.h"

#define _AMD64_ // required by windows
#include <libloaderapi.h> // GetProcAddress, LoadLibraryA

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
SharedLibraryLoad(const char* libraryPath, s_shared_library* sharedLibrary)
{
	if ((sharedLibrary->handle = LoadLibraryA(libraryPath)) == NULL)
		return RESULT_FAILURE;
	return RESULT_SUCCESS;
}

enum e_result
SharedLibraryGetSymbol(const s_shared_library* sharedLibrary, const char* symbolName, void** symbol)
{
	if ((*symbol = (void*)GetProcAddress(sharedLibrary->handle, symbolName)) == NULL)
		return RESULT_FAILURE;
	return RESULT_SUCCESS;
}

#pragma endregion