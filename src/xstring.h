#pragma once

#include "xstring.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

xstring XString(const char str[]); // Converts a char* to a xstring, allocated memory, use XStringFree() to free.
void XStringFree(const xstring xstr); // Only use with xstrings returned by XString() not XStringCopy.
xstring XStringCopy(char mem[], const sz memSize, const char str[]); // Will return NULL if mem(ory buffer) is too small.
xstring XStringCopyNoHash(char mem[], const sz memSize, const char str[]); // Same as above but without the hash.
sz XStringSize(const xstring xstr); // Returns the total memory footprint of this xstring, use in tandem with XStringCopy to shift your memory buffer.
u32 XStringLength(const xstring xstr); // Returns the length of the xstring
u32 XStringMatch(const xstring xstr1, const xstring xstr2); // Returns TRUE if both xstring match, and FALSE otherwise.

#pragma endregion