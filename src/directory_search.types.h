#pragma once

#include "common.h"

#include "xstring.types.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct s_directory_search_result
{
    void* mem;
    xstring* files;
    sz fileCount;
} s_directory_search_result;

typedef struct s_directory_view
{
    xstring path;
    xstring* files;
    sz fileCount;
} s_directory_view;

#pragma endregion