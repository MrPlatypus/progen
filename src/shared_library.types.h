#pragma once

#include "common.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct s_shared_library
{
	void* handle;
} s_shared_library;

#pragma endregion