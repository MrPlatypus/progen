#include "memory.h"

#include <stdlib.h> // free, malloc, realloc
#include <stdio.h> // printf

void*
_Malloc(char*const file, const char* function, u32 line, sz size)
{
#if defined(DEBUG)
    printf("\tMalloc  - %-27s %-25s %4i Malloc(%llu)\n", file, function, line, size);
#else
    (void)file; (void)function; (void)line;
#endif
	void* mem = malloc(size);
	return mem;
}

void*
_Realloc(char*const file, const char* function, u32 line, const void* mem, sz size)
{
#if defined(DEBUG)
    printf("\tRealloc - %-27s %-25s %4i Realloc(%p, %llu)\n", file, function, line, mem, size);
#else
    (void)file; (void)function; (void)line;
#endif
    void* newMem = realloc((void*)mem, size);
	return newMem;
}

void
_Free(char*const file, const char* function, u32 line, const void* mem)
{
#if defined(DEBUG)
    printf("\tFree    - %-27s %-25s %4i Free(%p)\n", file, function, line, mem);
#else
    (void)file; (void)function; (void)line;
#endif
	free((void*)mem);
}