#include "xarray.h"

#include "memory.h"

#include <stddef.h> // NULL

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

struct s_xarray_metadata
{
    u32 length;
};

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void*
XArray(u32 elementSize, const u32 length)
{
    struct s_xarray_metadata* metadata = Malloc(sizeof(struct s_xarray_metadata) + (elementSize * length));
    if (metadata == NULL)
        return NULL;
	
    metadata->length = length;
    
    return metadata + 1;
}

void*
XArrayGrow(void* array, u32 elementSize, const u32 length)
{
	struct s_xarray_metadata* metadata = NULL;
	if (array == NULL)
	{
		return XArray(elementSize, length);
	}
	else
	{
		metadata = (struct s_xarray_metadata*)array - 1;
		if (metadata->length < length)
		{
			metadata = Realloc(metadata, sizeof(struct s_xarray_metadata) + (elementSize * length));
			metadata->length = length;
		}
	}

	return metadata + 1;
}

u32
XArrayLength(const void* array)
{
	if (array == NULL)
		return 0;
	struct s_xarray_metadata* metadata = (struct s_xarray_metadata*)array - 1;
	return metadata->length;
}

void
XArrayFree(const void* array)
{
	struct s_xarray_metadata* metadata = (struct s_xarray_metadata*)array - 1;
    Free(metadata);
}

#pragma endregion