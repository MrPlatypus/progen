#pragma once

#include "common.h"

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef struct s_params
{
	char*const generatorPath;
	char*const* projects;
	u32 projectsCount;
	char*const* flags;
	u32 flagsCount;
    u8 verboseEnabled;
} s_params;

#pragma endregion