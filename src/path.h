#pragma once

#include "common.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// NOTE: All the following functions will ALLOCATE MEMORY to produce the string, even if no changes are required a new string will be made!
// You must therefor remember to free these strings when you are done with them!

const char* PathAbsolute(const char* pathRelative); // DO NOT change the working dir at runtime, or if you do change this function's implementation
const char* PathDirectory(const char* pathAbsolute);
const char* PathJoin(const char* pathLeft, const char* pathRight);

#pragma endregion