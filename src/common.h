#pragma once

#include <stdint.h>
#include <stddef.h> // size_t

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef signed char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

typedef float f32;
typedef double f64;

typedef size_t sz;
typedef uintptr_t ptr;

enum e_result
{
	RESULT_SUCCESS = 1,
	RESULT_FAILURE = 0,
	RESULT_TRUE = RESULT_SUCCESS,
	RESULT_FALSE = RESULT_FAILURE,
	RESULT_EXIT_SUCCESS = 0,
	RESULT_EXIT_FAILURE = 1
};

#define TRUE 1
#define FALSE 0

#define KILOBYTES(Value) ((Value) * 1024LL)
#define MEGABYTES(Value) (KILOBYTES(Value) * 1024LL)
#define GIGABYTES(Value) (MEGABYTES(Value) * 1024LL)
#define TERABYTES(Value) (GIGABYTES(Value)*1024LL)

#pragma endregion