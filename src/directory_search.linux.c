
#include "directory_search.h"

#include "memory.h"
#include "xstring.h"

#include <string.h> // memcpy
#include <dirent.h> // opendir, readdir, closedir

#include <stdio.h> //printf

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
DirectoryPathIsValid(const char directoryPath[])
{
    DIR* dir= opendir(directoryPath);
	if (dir == NULL)
	{
		return RESULT_FALSE;
	}
    closedir(dir);
    return RESULT_TRUE;
}

struct s_directory_search_result
DirectorySearch(const char directoryPath[], const char* frontFilter, const char* backFilter)
{
    DIR* dir= opendir(directoryPath);
    
    static const sz arenaChunkSize = KILOBYTES(1);
    
	// Size == bytes
    
	sz arrayIndex = 0;
	sz arrayCurrentSize = 0;
	sz arrayMaxSize = arenaChunkSize;
	sz* stringsOffset = Malloc(arrayMaxSize);
    
	sz bufferCurrentSize = 0;
	sz bufferMaxSize = arenaChunkSize;
	char* buffer = Malloc(bufferMaxSize);
    
    struct dirent* dirent;
    
    if (frontFilter == NULL && backFilter == NULL)
    {
        while ((dirent = readdir(dir)) != NULL)
        {
            if (dirent->d_type == DT_REG)
            {
                if (arrayCurrentSize >= arrayMaxSize)
                {
                    arrayMaxSize += arenaChunkSize;
                    stringsOffset = Realloc(stringsOffset, arrayMaxSize);
                }
                {
                    xstring xstr;
                    while ((xstr = XStringCopyNoHash(buffer + bufferCurrentSize, bufferMaxSize - bufferCurrentSize, dirent->d_name)) == NULL)
                    {
                        bufferMaxSize += arenaChunkSize;
                        buffer = Realloc(buffer, bufferMaxSize);
                    }
                    bufferCurrentSize += XStringSize(xstr); 
                    
                    ptrdiff_t offset = xstr - buffer;
                    stringsOffset[arrayIndex++] = offset;
                    arrayCurrentSize += sizeof(char*);
                }
            }
        }
    }
    else if (frontFilter == NULL && backFilter != NULL)
    {
        u32 backFilterLength = strlen(backFilter);
        while ((dirent = readdir(dir)) != NULL)
        {
            if (dirent->d_type == DT_REG)
            {
                if (arrayCurrentSize >= arrayMaxSize)
                {
                    arrayMaxSize += arenaChunkSize;
                    stringsOffset = Realloc(stringsOffset, arrayMaxSize);
                }
                {
                    u32 stringLength = strlen(dirent->d_name);
                    if (stringLength <= backFilterLength)
                    {
                        continue;
                    }
                    
                    if (strcmp(dirent->d_name + (stringLength - backFilterLength), backFilter) != 0)
                    {
                        continue;
                    }
                    
                    xstring xstr;
                    while ((xstr = XStringCopyNoHash(buffer + bufferCurrentSize, bufferMaxSize - bufferCurrentSize, dirent->d_name)) == NULL)
                    {
                        bufferMaxSize += arenaChunkSize;
                        buffer = Realloc(buffer, bufferMaxSize);
                    }
                    bufferCurrentSize += XStringSize(xstr); 
                    
                    ptrdiff_t offset = xstr - buffer;
                    stringsOffset[arrayIndex++] = offset;
                    arrayCurrentSize += sizeof(char*);
                }
            }
        }
    }
    else if (frontFilter != NULL && backFilter == NULL)
    {
        u32 frontFilterLength = strlen(frontFilter);
        while ((dirent = readdir(dir)) != NULL)
        {
            if (dirent->d_type == DT_REG)
            {
                if (arrayCurrentSize >= arrayMaxSize)
                {
                    arrayMaxSize += arenaChunkSize;
                    stringsOffset = Realloc(stringsOffset, arrayMaxSize);
                }
                {
                    u32 stringLength = strlen(dirent->d_name);
                    if (stringLength <= frontFilterLength)
                    {
                        continue;
                    }
                    
                    if (strncmp(dirent->d_name, frontFilter, frontFilterLength) != 0)
                    {
                        continue;
                    }
                    
                    xstring xstr;
                    while ((xstr = XStringCopyNoHash(buffer + bufferCurrentSize, bufferMaxSize - bufferCurrentSize, dirent->d_name)) == NULL)
                    {
                        bufferMaxSize += arenaChunkSize;
                        buffer = Realloc(buffer, bufferMaxSize);
                    }
                    bufferCurrentSize += XStringSize(xstr); 
                    
                    ptrdiff_t offset = xstr - buffer;
                    stringsOffset[arrayIndex++] = offset;
                    arrayCurrentSize += sizeof(char*);
                }
            }
        }
    }
    else
    {
        u32 backFilterLength = strlen(backFilter);
        u32 frontFilterLength = strlen(frontFilter);
        while ((dirent = readdir(dir)) != NULL)
        {
            if (dirent->d_type == DT_REG)
            {
                if (arrayCurrentSize >= arrayMaxSize)
                {
                    arrayMaxSize += arenaChunkSize;
                    stringsOffset = Realloc(stringsOffset, arrayMaxSize);
                }
                {
                    u32 stringLength = strlen(dirent->d_name);
                    if (stringLength <= backFilterLength + frontFilterLength)
                    {
                        continue;
                    }
                    
                    if (strcmp(dirent->d_name + (stringLength - backFilterLength), backFilter) != 0)
                    {
                        continue;
                    }
                    
                    if (strncmp(dirent->d_name, frontFilter, frontFilterLength) != 0)
                    {
                        continue;
                    }
                    
                    xstring xstr;
                    while ((xstr = XStringCopyNoHash(buffer + bufferCurrentSize, bufferMaxSize - bufferCurrentSize, dirent->d_name)) == NULL)
                    {
                        bufferMaxSize += arenaChunkSize;
                        buffer = Realloc(buffer, bufferMaxSize);
                    }
                    bufferCurrentSize += XStringSize(xstr); 
                    
                    ptrdiff_t offset = xstr - buffer;
                    stringsOffset[arrayIndex++] = offset;
                    arrayCurrentSize += sizeof(char*);
                }
            }
        }
    }
    closedir(dir);
    
    struct s_directory_search_result result;
    result.mem = NULL;
    result.files = NULL;
    result.fileCount = 0;
    
    // "Shrinking"
    if (arrayIndex > 0)
    {
        result.mem = Malloc(bufferCurrentSize);
        memcpy(result.mem, buffer, bufferCurrentSize);
        Free(buffer);
        
        result.files = Malloc(arrayIndex * sizeof(char*));
        for (sz i = 0; i < arrayIndex; ++i)
        {
            result.files[i] = result.mem + stringsOffset[i];
        }
        Free(stringsOffset);
        
        result.fileCount = arrayIndex;
    }
    else
    {
        Free(buffer);
        Free(stringsOffset);
    }
    
    return result;
}

void
DirectorySearchFree(struct s_directory_search_result* result)
{
    if (result->fileCount > 0)
    {
        Free(result->mem);
        result->mem = NULL;
        Free(result->files);
        result->files = NULL;
    }
}

struct s_directory_view
DirectoryView(const char* path)
{
    struct s_directory_view view;
    view.files = NULL;
    view.fileCount = 0;
    view.path = XString(path);
    return view;
}

void
DirectoryViewPopulate(struct s_directory_view* view, const struct s_directory_search_result* result)
{
    view->files = Realloc(view->files, sizeof(char*) * (view->fileCount + result->fileCount));
    memcpy(view->files + view->fileCount, result->files, sizeof(char*) * result->fileCount);
    view->fileCount += result->fileCount;
}

void
DirectoryViewExclude(struct s_directory_view* view, xstring frontFilter, xstring backFilter)
{
    if (frontFilter == NULL && backFilter == NULL)
    {
        return;
    }
    else if (frontFilter == NULL && backFilter != NULL)
    {
        u32 backFilterLength = strlen(backFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < backFilterLength)
            {
                ++i;
                continue;
            }
            
            if (strcmp(file + (stringLength - backFilterLength), backFilter) != 0)
            {
                ++i;
                continue;
            }
            
            --view->fileCount;
            view->files[i] = view->files[view->fileCount];
        }
    }
    else if (frontFilter != NULL && backFilter == NULL)
    {
        u32 frontFilterLength = strlen(frontFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < frontFilterLength)
            {
                ++i;
                continue;
            }
            
            if (strncmp(file, frontFilter, frontFilterLength) != 0)
            {
                ++i;
                continue;
            }
            
            --view->fileCount;
            view->files[i] = view->files[view->fileCount];
        }
    }
    else
    {
        u32 frontFilterLength = strlen(frontFilter);
        u32 backFilterLength = strlen(backFilter);
        for (sz i = 0; i < view->fileCount;)
        {
            xstring file = view->files[i];
            u32 stringLength = XStringLength(file);
            
            if (stringLength < (frontFilterLength + backFilterLength))
            {
                ++i;
                continue;
            }
            
            if (strcmp(file + (stringLength - backFilterLength), backFilter) != 0)
            {
                ++i;
                continue;
            }
            
            if (strncmp(file, frontFilter, frontFilterLength) != 0)
            {
                ++i;
                continue;
            }
            
            view->files[i] = view->files[view->fileCount - 1];
            view->fileCount -= 1;
        }
    }
}

void
DirectoryViewFree(struct s_directory_view* view)
{
    XStringFree(view->path);
    view->path = NULL;
    if (view->fileCount > 0)
    {
        Free(view->files);
        view->files = NULL;
        view->fileCount = 0;
    }
};