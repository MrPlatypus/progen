#include "file_system.h"

#include "memory.h"

#include <stdlib.h> // malloc
#include <stdio.h> // feof, fopen_s, fread, fseek, ftell, printf

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_file_system_result
FileOpen(const char* path, s_file* file)
{
    file->handle= fopen(path, "rb");
	return (file->handle != NULL) ? FS_SUCCESS : FS_FAILURE;
}

enum e_file_system_result
FileRead(const s_file* file, s_file_buffer* fileBuffer)
{
	// Querying size
	fseek(file->handle, 0, SEEK_END);
	long fileSizeBytes = ftell(file->handle);
	fseek(file->handle, 0, SEEK_SET);
	// Allocating memory for the buffer
	if ((fileBuffer->data = (char*)Malloc(sizeof(char) * (fileSizeBytes + 1))) == NULL) return FS_FAILURE;
	fileBuffer->size = fileSizeBytes + 1;
	// Read and null termination
    sz readAmount = fread(fileBuffer->data, sizeof(char), fileSizeBytes, file->handle);
	fileBuffer->data[readAmount] = 0;
	// End
	return (readAmount > 0 || feof((FILE*)file->handle) != 0) ? FS_SUCCESS : FS_FAILURE;
}

#pragma endregion