#include "shared_library.h"

#include <dlfcn.h> // dlopen dlclose dlsym
#include <stdio.h> // printf

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
SharedLibraryLoad(const char* libraryPath, s_shared_library* sharedLibrary)
{
	if ((sharedLibrary->handle = dlopen(libraryPath, RTLD_LAZY)) == NULL)
    {
        printf("%s\n", dlerror());
        return RESULT_FAILURE;
    }
    
	return RESULT_SUCCESS;
}

enum e_result
SharedLibraryGetSymbol(const s_shared_library* sharedLibrary, const char* symbolName, void** symbol)
{
	if ((*symbol = (void*)dlsym(sharedLibrary->handle, symbolName)) == NULL)
		return RESULT_FAILURE;
	return RESULT_SUCCESS;
}

#pragma endregion