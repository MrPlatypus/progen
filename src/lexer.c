#include "lexer.h"

#include "file_system.types.h"

#include "memory.h"

#include <stdio.h> // printf
#include <stdlib.h> // malloc, realloc
#include <string.h> //strcmp, memcpy

#pragma region TYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

typedef enum e_lexer_result
{
	LEXER_SYNTAX_OK,
	LEXER_SYNTAX_ERROR
} e_lexer_result;

#pragma endregion

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static char*
LexerSkipWhitespaceAndComments(char* cursor)
{
	while (*cursor == ' ' || *cursor == '\t' || *cursor == '\r' || *cursor == '\n' || *cursor == '#')
	{
		if (*cursor == '#')
		{
			while (*cursor != '\n' && *cursor != '\0') ++cursor;
			if (*cursor == '\n') ++cursor;
		}
		else
		{
			++cursor;
		}
	}
	return cursor;
}

static char*
LexerFindEndOfLineToken(char* cursor)
{
	char* checkpoint = cursor;
	while (*cursor != '\n' && *cursor != '#' && *cursor != '\0')
	{
		if (*cursor != ' ' && *cursor != '\t' && *cursor != '\r') checkpoint = cursor;
		++cursor;
	}
	return ++checkpoint;
}

static enum e_lexer_result
LexerProcessArguments(struct s_lexer_token* lexerToken, char delimiterStart, char delimiterEnd)
{
	char* checkpointStart = lexerToken->name;
	char* checkpointEnd = lexerToken->name;
    
	// Checking if start and end delimiters are present and saving their positions
	{
		char* cursor = lexerToken->name;
		while (*cursor != delimiterStart && *cursor != '\0') ++cursor;
		if (*cursor == '\0')
		{
			printf("Error while parsing arguments: %s\nFailed to find the opening delimiter %c\n", lexerToken->name, delimiterStart);
			return LEXER_SYNTAX_ERROR;
		}
		checkpointStart = cursor;
		while (*cursor != delimiterEnd && *cursor != '\0') ++cursor;
		if (*cursor == '\0')
		{
			printf("Error while parsing arguments: %s\nFailed to find the closing delimiter %c\n", lexerToken->name, delimiterStart);
			return LEXER_SYNTAX_ERROR;
		}
		checkpointEnd = cursor;
	}
    
	// Checking for space between both delimiters and unwanted characters after the ending delimiter
	{
		if (checkpointStart + 1 == checkpointEnd)
		{
			printf("Error while parsing arguments: %s\nNo values were found between the start and end delimiters\n", lexerToken->name);
			return LEXER_SYNTAX_ERROR;
		}
		if (checkpointEnd[1] != '\0')
		{
			printf("Error while parsing arguments: %s\nUnexpected character after ending delimiter\n", lexerToken->name);
			return LEXER_SYNTAX_ERROR;
		}
		*checkpointStart = '\0'; // Replace opening/ending delemiters with a \0 as we are reusing this buffer for the arguments array
		*checkpointEnd = '\0';
		++checkpointStart;
	}
    
	// Counting number of arguments (there is one by default)
	{
		lexerToken->paramsCount = 1;
		for(char* cursor = checkpointStart; cursor < checkpointEnd; ++cursor)
		{
			if (*cursor == ',') ++lexerToken->paramsCount;
		}
		if ((lexerToken->paramsArray = (char**)Malloc(sizeof(*lexerToken->paramsArray) * lexerToken->paramsCount)) == NULL) return LEXER_SYNTAX_ERROR;
	}
    
	// Copying arguments into char* array
	{
		unsigned int paramsArrayIndex = 0;
		if (lexerToken->paramsCount > 1)
		{
			for (char* cursor = checkpointStart; cursor < checkpointEnd; ++cursor)
			{
				if (*cursor == ',')
				{
					lexerToken->paramsArray[paramsArrayIndex++] = checkpointStart;
					checkpointStart = cursor+1;
					*cursor = '\0';
				}
			}
		}
		lexerToken->paramsArray[lexerToken->paramsCount-1] = checkpointStart;
	}
    
	return LEXER_SYNTAX_OK;
}

static enum e_lexer_result
LexerProcessToken(struct s_lexer_token* lexerToken)
{
    static char*const tokenHeader = "<HEADER>";
    static char*const tokenProperties = "<PROPERTIES>";
    
	lexerToken->type = LEXER_TOKEN_NONE;
	switch (*lexerToken->name)
	{
		case '\0':
		{
			lexerToken->type = LEXER_TOKEN_EOF;
		} break;
		case '<':
		{
			if (strcmp(tokenHeader, lexerToken->name) == 0)
			{
				lexerToken->type = LEXER_TOKEN_CATEGORY_HEADER;
				static const unsigned int headerNameLength = sizeof("<HEADER>") / sizeof(char);
				lexerToken->name[headerNameLength - 2] = '\0'; // Removing the ending '>'
			}
			else if (strcmp(tokenProperties, lexerToken->name) == 0)
			{
				lexerToken->type = LEXER_TOKEN_CATEGORY_PROPERTIES;
				static const unsigned int headerNameLength = sizeof("<PROPERTIES>") / sizeof(char);
				lexerToken->name[headerNameLength - 2] = '\0'; // Removing the ending '>'
			}
			else
			{
				lexerToken->type = LEXER_TOKEN_NONE;
				printf("Unsupported category found: %s\n'<' at the start of a line is reserved for declaring the start of a category, either <HEADER> or <PROPERTIES>\n", lexerToken->name);
				return LEXER_SYNTAX_ERROR;
			}
			// Shifting everything over to remove the '<' at the start
			lexerToken->name = lexerToken->name + 1;
		} break;
		case '[':
		{
			lexerToken->type = LEXER_TOKEN_FILTER;
			LexerProcessArguments(lexerToken, '[', ']');
			if (lexerToken->paramsCount != 2)
			{
				printf("Too many arguments for filter: %s\n'[' at the start of a line is reserved for declaring a filter, ex: [WIN32,DEBUG]\n", lexerToken->name);
				return LEXER_SYNTAX_ERROR;
			}
		} break;
		default:
		{
			lexerToken->type = LEXER_TOKEN_FUNCTION;
			LexerProcessArguments(lexerToken, '(', ')');
		} break;
	}
	return LEXER_SYNTAX_OK;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result
LexerInit(const s_file_buffer* fileBuffer, s_lexer* lexer)
{
	lexer->start = fileBuffer->data;
	lexer->cursor = fileBuffer->data;
	return RESULT_SUCCESS;
}

enum e_result
LexerGetNextToken(s_lexer* lexer, s_lexer_token* lexerToken)
{
	lexer->cursor = LexerSkipWhitespaceAndComments(lexer->cursor);
	char* seperator = LexerFindEndOfLineToken(lexer->cursor);
    
	// Copy memory
	size_t size = seperator - lexer->cursor;
#if 1
	if ((lexerToken->memory = Realloc(lexerToken->memory, size + 1)) == NULL)
		return RESULT_FAILURE;
#else
    if (lexerToken->memory != NULL)
        Free(lexerToken->memory);
    lexerToken->memory = Malloc(size);
#endif
	lexerToken->name = lexerToken->memory;
	memcpy(lexerToken->name, lexer->cursor, size);
	lexerToken->name[size] = '\0';
    
	if (LexerProcessToken(lexerToken) != LEXER_SYNTAX_OK)
		return RESULT_FAILURE;
	lexer->cursor = seperator;
	return RESULT_SUCCESS;
}

#pragma endregion