#include "common.h"

#include "param_parser.types.h"

#pragma region API - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

enum e_result ProGenRun(struct s_params*);

#pragma endregion