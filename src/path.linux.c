#include "path.h"

#include "memory.h"

#include <assert.h> // assert
#include <stdio.h> // printf
#include <string.h> // strlen
#include <unistd.h> // getcwd

#pragma region PRIVATE_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

static const char*
PathNormalize(const char* path)
{
	(void)path;
	return NULL;
}

#pragma endregion

#pragma region PUBLIC_FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

const char*
PathAbsolute(const char* pathRelative)
{
	static char* pathWorkingDirectory;
	static sz pathWorkingDirectoryLength;
	
    if (pathWorkingDirectory == NULL)
	{
        while ((pathWorkingDirectory = getcwd(pathWorkingDirectory, pathWorkingDirectoryLength)) == NULL)
        {
            pathWorkingDirectoryLength += 1024;
            pathWorkingDirectory = Realloc(pathWorkingDirectory, pathWorkingDirectoryLength);
        }
        pathWorkingDirectoryLength = strlen(pathWorkingDirectory);
        char* buffer = Malloc(pathWorkingDirectoryLength + 2);
        memcpy(buffer, pathWorkingDirectory, pathWorkingDirectoryLength);
        Free(pathWorkingDirectory);
        
        buffer[pathWorkingDirectoryLength] = '/';
        buffer[pathWorkingDirectoryLength + 1] = '\0',
        pathWorkingDirectoryLength += 1;
        
        pathWorkingDirectory = buffer;
	}
	sz pathRelativeLength = strlen(pathRelative);
	char* pathFull = NULL;
	if (pathRelative[0] == '/')
    {
        pathFull = Malloc(pathRelativeLength + 1);
        memcpy(pathFull, pathRelative, pathRelativeLength);
        pathFull[pathRelativeLength] = '\0';
        for (sz i = 0; i < pathWorkingDirectoryLength; ++i)
        {
            if (pathWorkingDirectory[i] == '\\')
                pathWorkingDirectory[i] = '/';
        }
    }
    else
    {
        pathFull = Malloc(pathWorkingDirectoryLength + pathRelativeLength + 1);
        memcpy(pathFull, pathWorkingDirectory, pathWorkingDirectoryLength);
        memcpy(pathFull + pathWorkingDirectoryLength, pathRelative, pathRelativeLength);
        pathFull[pathWorkingDirectoryLength + pathRelativeLength] = '\0';
        for (sz i = 0; i < pathWorkingDirectoryLength; ++i)
        {
            if (pathWorkingDirectory[i] == '\\')
                pathWorkingDirectory[i] = '/';
        }
    }
    return pathFull;
}

const char*
PathDirectory(const char* pathFile)
{
	sz pathLength = strlen(pathFile);
	sz checkpoint = 0;
	for (sz i = 0; i < pathLength; ++i)
	{
		if (pathFile[i] == '/')
			checkpoint = i + 1;
	}
	char* pathDirectory = NULL;
	if (checkpoint == 0)
	{
		assert(0 && "Path should be absolute and therefor should contain at least one seperator !");
	}
	else
	{
		pathDirectory = Malloc(checkpoint + 1);
		memcpy(pathDirectory, pathFile, checkpoint);
		pathDirectory[checkpoint] = '\0';
	}
	return pathDirectory;
}

const char*
PathJoin(const char* pathLeft, const char* pathRight)
{
	sz pathLeftLength = strlen(pathLeft);
	sz pathRightLength = strlen(pathRight);
	char* pathFusion = Malloc(pathLeftLength + pathRightLength + 1);
	memcpy(pathFusion, pathLeft, pathLeftLength);
	memcpy(pathFusion + pathLeftLength, pathRight, pathRightLength);
	pathFusion[pathLeftLength + pathRightLength] = '\0';
	for (sz i = pathLeftLength; i < pathLeftLength + pathRightLength; ++i)
	{
		if (pathFusion[i] == '\\')
			pathFusion[i] = '/';
	}
	return pathFusion;
}

#pragma endregion