#!/bin/bash 

PATH_ROOT=$(dirname $(readlink -f "$0"))"/"
PATH_SRC_DIR="$PATH_ROOT""src/"
PATH_DATA_DIR="$PATH_ROOT""data/"
PATH_BUILD_DIR="$PATH_ROOT""__build__/"

PATH_SOURCE="directory_search.linux.c"
PATH_SOURCE+=" file_system.linux.c"
PATH_SOURCE+=" lexer.c"
PATH_SOURCE+=" main.c"
PATH_SOURCE+=" memory.c"
PATH_SOURCE+=" param_parser.c"
PATH_SOURCE+=" path.linux.c"
PATH_SOURCE+=" progen.c"
PATH_SOURCE+=" shared_library.linux.c"
PATH_SOURCE+=" xarray.c"
PATH_SOURCE+=" xstring.c"

NAME_OUTPUT=progen.out

DEFINES=
CFLAGS=-I"$PATH_ROOT" $DEFINES
LFLAGS=

if [ ! -d "$PATH_BUILD_DIR" ]
then
	mkdir "$PATH_BUILD_DIR"
fi

pushd $PATH_SRC_DIR > NUL
gcc $CFLAGS $PATH_SOURCE $LFLAGS -o$PATH_BUILD_DIR$NAME_OUTPUT
popd > NUL