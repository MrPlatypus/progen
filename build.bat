@ECHO off

REM - - - - TIME - - - -
SETLOCAL EnableDelayedExpansion

SET "startTime=%time: =0%"
REM - - - - TIME - - - -



SET PATH_ROOT=%~dp0
SET PATH_SRC_DIR=%PATH_ROOT%src\
SET PATH_DATA_DIR=%PATH_ROOT%data\
SET PATH_BUILD_DIR=%PATH_ROOT%__build__\

SET PATH_SOURCE="directory_search.windows.c"^
                "file_system.windows.c"^
                "lexer.c"^
                "main.c"^
                "memory.c"^
                "param_parser.c"^
				"path.windows.c"^
                "progen.c"^
                "shared_library.windows.c"^
				"xarray.c"^
                "xstring.c"
SET NAME_OUTPUT=progen.exe

SET DEFINES=/D_CRT_SECURE_NO_WARNINGS /DUNICODE /D_UNICODE

SET CFLAGS=/nologo /W4 /wd4201 /wd4456 /Od /Z7 /MT /MP /Fo:%PATH_BUILD_DIR% /I"%PATH_ROOT%\" %DEFINES%
SET LFLAGS=/nologo /INCREMENTAL:NO /DEBUG:FASTLINK

IF NOT EXIST %PATH_BUILD_DIR% MKDIR %PATH_BUILD_DIR%

pushd %PATH_SRC_DIR%
cl.exe %CFLAGS% %PATH_SOURCE% /link %LFLAGS% /OUT:%PATH_BUILD_DIR%%NAME_OUTPUT%
popd



REM - - - - TIME - - - -
SET "endTime=%time: =0%"

REM Get elapsed time:
SET "end=!endTime:%time:~8,1%=%%100)*100+1!"  &  SET "start=!startTime:%time:~8,1%=%%100)*100+1!"
SET /A "elap=((((10!end:%time:~2,1%=%%100)*60+1!%%100)-((((10!start:%time:~2,1%=%%100)*60+1!%%100)"

REM Convert elapsed time to HH:MM:SS:CC format:
SET /A "cc=elap%%100+100,elap/=100,ss=elap%%60+100,elap/=60,mm=elap%%60+100,hh=elap/60+100"

REM ECHO Start:    %startTime%
REM ECHO End:      %endTime%
ECHO Elapsed:  %hh:~1%%time:~2,1%%mm:~1%%time:~2,1%%ss:~1%%time:~8,1%%cc:~1%
REM - - - - TIME - - - -

REM - - - - DOCUMENTATION - - - -
	REM /nologo Suppresses the display of the copyright banner when the compiler starts up.
	REM /FC 	Forces the compiler to display the full path of the source code
	REM /W4     Sets the warning level to 4.
	REM /wd4201 Suppresses a warning. Nonstandard extension used : nameless struct/union
	REM /wd4456 Suppresses a warning. The declaration of identifier in the local scope hides the declaration of the previous local declaration of the same name.
	REM /Od     Disable all optimisations.
	REM /O2     Create the fastest code in the majority of cases.
	REM /Z7     Generates less pdb files (implicitely triggers the generation of pdb files).
	REM /MT     Use the multithread, static version of the run-time library.
	REM /MP     Enables multi-process compilation.
	REM /Fo     Specifies an object (.obj) file name or directory to be used instead of the default.

	REM /nologo               Suppresses the display of the copyright banner when the compiler starts up.
	REM /INCREMENTAL:NO       Controls how the linker handles incremental linking.
	REM /DEBUG:FASTLINK       Generate light .pdb info.

	REM /OUT                  Specifies the output's name.
REM - - - - DOCUMENTATION - - - -